const INITIAL_STATE = {
  region: {
    latitude: 45.52220671242907,
    longitude: -122.6653281029795,
    latitudeDelta: 0.04864195044303443,
    longitudeDelta: 0.040142817690068,
  }
};

const mapReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'UPDATE_REGION':
      return {...state, region: action.payload}
    default:
      return state
  }
};

export default mapReducer;

