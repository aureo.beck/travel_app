export const updateRegion = region => (
    {
      type: 'UPDATE_REGION',
      payload: region,
    }
  );