export const updateLanguage = language => (
    {
      type: 'UPDATE_LANGUAGE',
      payload: language,
    }
  );