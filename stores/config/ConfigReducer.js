const INITIAL_STATE = {
  language: 'pt',
};

const configReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'UPDATE_LANGUAGE':
      const newState = {...state, language: action.payload}
      return newState;
    default:
      return state
  }
};

export default configReducer;
