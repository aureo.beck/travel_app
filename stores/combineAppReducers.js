import { combineReducers } from "redux";
import configReducer from "./config/ConfigReducer";
import mapReducer from "./map/MapReducer";

const combineAppReducers = () => {
    return combineReducers({
        config: configReducer,
        map: mapReducer,
      });
}

export default combineAppReducers;
