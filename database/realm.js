import { Platform } from 'react-native';
import Realm from 'realm';

import * as schemas from './schemas';
import * as migrations from './migrations';
import { SCHEMA_VERSION, ANDROID_SCHEMA_VERSION } from './version';
                                                                                                                                                                       
export const REALM_ANDROID_FILE_NAME = 'android.realm';
export const REALM_DEFAULT_PATH = Realm.defaultPath;

let _realmInstance;

function getSchemaFilesWithVersion(version) {
    const schemaFiles = schemas[`schemasV${version}`];

    if (!schemaFiles) {
        throw new Error(`[Realm] Não encontrado arquivos de schema para versão ${version}`);
    }
    return schemaFiles;
}

function getMigrationFunctionWithVersion(version) {
    const migrationFunction = migrations[`migrationV${version}`];

    if (!migrationFunction) {
        throw new Error(`Não encontrado função de migração para versão ${version}`);
    }
    return migrationFunction;
}

function getCurrentSchemaVersion(encryptionKey) {
    return Realm.schemaVersion(REALM_DEFAULT_PATH, encryptionKey);
}

function realmConfigWithVersion(version, encryptionKey) {
    if (!encryptionKey) {
        throw new Error('[Realm] Falha ao criar configuração do Realm: sem chave de criptografia');
    }

    const schemaFiles = getSchemaFilesWithVersion(version);
    const migrationFunction = getMigrationFunctionWithVersion(version);

    return {
        schemaVersion: version,
        schema: schemaFiles,
        migration: migrationFunction,
        encryptionKey: encryptionKey,
        path: REALM_DEFAULT_PATH,
    };
}

function isMigrationRequired(encryptionKey) {
    if (!encryptionKey) {
        throw new Error('[Realm] Falha ao verificar versão do banco de dados: sem chave de criptografia');
    }

    const currentSchemaVersion = getCurrentSchemaVersion(encryptionKey);
    if (currentSchemaVersion === -1) {
        console.info('[Realm] Migração não requerida pois arquivo de banco de dados não existe');
        return false;
    }

    if (SCHEMA_VERSION < currentSchemaVersion) {
        throw new Error(`[Realm] Falha na migração: tentando fazer downgrade da versão do banco de dados. Arquivo Realm está na versão ${currentSchemaVersion} e tentando migrar para ${SCHEMA_VERSION}`);
    }

    return SCHEMA_VERSION > currentSchemaVersion;
}

function migrateToVersion(version, encryptionKey) {
    console.info(`[Realm] Migrando banco de dados para versão ${version}`);
    const temporaryMigrationRealm = new Realm(realmConfigWithVersion(version, encryptionKey));
    temporaryMigrationRealm.close();
}

function migrate(encryptionKey) {
    const isRequired = isMigrationRequired(encryptionKey);
    if (!isRequired) {
        throw new Error('[Realm] A migração não era necessária, considere chamar isMigrationRequired antes');
    }

    const oldVersion = getCurrentSchemaVersion(encryptionKey);
    console.info(`[Realm] Iniciando migração da versão ${oldVersion} para ${SCHEMA_VERSION}`);

    const nextVersion = oldVersion + 1;
    for (let i = nextVersion; i <= SCHEMA_VERSION; i++) {
        migrateToVersion(i, encryptionKey);
    }

    console.info('[Realm] Migração finalizada');
}

function createNotificationRealm(encryptionKey) {
    if (!encryptionKey) {
        throw new Error('[Realm] Falha ao criar Realm de notificações do Android: sem chave de criptografia');
    }

    const realmConfigAndroid = {
        schemaVersion: ANDROID_SCHEMA_VERSION,
        schema: notificationSchema,
        encryptionKey: encryptionKey,
        path: REALM_ANDROID_FILE_NAME,
    };

    /*
        Prepara base de dados para uso do nativo.
        É necessário criar um arquivo Realm separada por conta de problemas decorrentes a usar o mesmo arquivo 
        tanto no Javascript como no nativo Android. Em alguns casos em que os dois tentavam escrever no arquivo, 
        alguns problemas aconteciam:
            - Se nativo estava escrevendo e Javascript tentava escrever, Javascript não encontrava tabelas do banco.
            - Se Javascript estava escrevendo e nativo tentava escrever, aplicativo fechava com erro no Realm Core.
    */
    const realmAndroid = new Realm(realmConfigAndroid);
    realmAndroid.close();
}

const init = (encryptionKey, onMigrationInit, onMigrationFinish) => {
    if (!encryptionKey) {
        throw new Error('[Realm] Falha ao tentar iniciar banco de dados: sem chave de criptografia');
    }

    // if (isMigrationRequired(encryptionKey)) {
    //     if (!onMigrationInit || !onMigrationFinish) {
    //         throw new Error('[Realm] Falha na migração de dados: sem funções de callback para progresso da migração');
    //     }

    //     onMigrationInit();
    //     migrate(encryptionKey);
    //     onMigrationFinish();
    // }

    if (!_realmInstance) {
        const realmConfig = realmConfigWithVersion(SCHEMA_VERSION, encryptionKey);
        _realmInstance = new Realm(realmConfig);
    }
};

const getRealm = () => {
    if (!_realmInstance) {
        console.warn('Falha ao resgatar instância do Realm. Você inicializou?');
    }
    return _realmInstance;
};

const deleteAll = () => {
    getRealm().write(() => {
        getRealm().deleteAll();
    });
};

const stop = () => {
    if (_realmInstance) {
        const instance = _realmInstance;
        _realmInstance = undefined;
        instance.close();
    }
};

export {
    init,
    getRealm,
    deleteAll,
    stop,
};
