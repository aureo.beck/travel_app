import { writePlaces } from "./operations/place";
import { initialPlaces } from "../constants/initialData";

export function insertInitialData() {
    writePlaces(initialPlaces);
};