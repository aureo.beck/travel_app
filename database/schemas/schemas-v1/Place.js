const Place = {
    name: 'Place',
    primaryKey: 'id',
    properties: {
        id: { type: 'string' },
        title: { type: 'string' },
        description: { type: 'string' },
        rating: { type: 'int' },
        imageUrl: { type: 'string' },
        lat: { type: 'double' },
        lng: { type: 'double' },
        tips: { type: 'list', objectType: 'Tip' },
        questions: { type: 'list', objectType: 'Question' },
    },
};

export {
    Place,
};
