const Question = {
    name: 'Question',
    primaryKey: 'id',
    properties: {
        id: { type: 'string' },
        placeId: { type: 'string' },
        title: { type: 'string' },
        description: { type: 'string' },
        rating: { type: 'int' },
    },
};

export {
    Question,
};
