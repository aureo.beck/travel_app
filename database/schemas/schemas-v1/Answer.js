const Answer = {
    name: 'Answer',
    primaryKey: 'id',
    properties: {
        id: { type: 'string' },
        questionId: { type: 'string' },
        title: { type: 'string' },
        description: { type: 'string' },
        rating: { type: 'int' },
    },
};

export {
    Answer,
};
