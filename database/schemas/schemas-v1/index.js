import { Place } from './Place';
import { Answer } from './Answer';
import { Question } from './Question';
import { Tip } from './Tip';

export default [
    Place,
    Answer,
    Question,
    Tip,
];
