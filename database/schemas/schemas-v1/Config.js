const Config = {
    name: 'Config',
    primaryKey: 'id',
    properties: {
        type: { type: 'string' },
        data: { type: 'Object' },
    },
};

export {
    Config,
};
