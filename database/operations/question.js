import { getRealm } from './../realm';

const schemaName = 'Question';

const createRealmQuestion = ({ id, placeId, title, description, rating, answers }) => {
    return {
        id: String(id),
        placeId: String(placeId),
        title,
        description,
        rating,
        answers,
    };
};

// TODO: USE CORRECT QUERY
const retrieveAllQuestions = () => {
    return getRealm().objects(schemaName);
};

const deleteQuestions = () => {
    getRealm().delete(retrieveAllQuestions());
};

const writeQuestion = question => {
    getRealm().write(() => {
        getRealm().create(schemaName, createRealmQuestion(question), true);
    });
};

const getQuestionByPlaceId = placeId => {
    return retrieveAllQuestions().filter(tip => tip.placeId == placeId);
};

const getQuestionById = id => {
    return retrieveAllQuestions().find(question => question.id == id);
};

export {
    writeQuestion,
    getQuestionById,
    getQuestionByPlaceId,
    retrieveAllQuestions,
};
