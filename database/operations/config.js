import { getRealm } from './../realm';

const schemaName = 'Config';

const createRealmConfig = ({ id, type, data }) => {
    return {
        id: String(id),
        type,
        data,
    };
};

// TODO: USE CORRECT QUERY
const retrieveAllConfigs = () => {
    return getRealm().objects(schemaName);
};

const deleteConfigs = () => {
    getRealm().delete(retrieveAllConfigs());
};

const writeConfig = config => {
    getRealm().write(() => {
        getRealm().create(schemaName, createRealmConfig(config), true);
    });
};

const getConfigByType = type => {
    return retrieveAllConfigs().filter(config => config.type == type);
};

export {
    writeConfig,
    retrieveAllConfigs,
    getConfigByType
};
