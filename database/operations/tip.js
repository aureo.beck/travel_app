import { getRealm } from './../realm';

const schemaName = 'Tip';

const createRealmTip = ({ id, placeId, title, description, rating }) => {
    return {
        id: String(id),
        placeId: String(placeId),
        title,
        description,
        rating,
    };
};

// TODO: USE CORRECT QUERY
const retrieveAllTips = () => {
    return getRealm().objects(schemaName);
};

const deleteTips = () => {
    getRealm().delete(retrieveAllTips());
};

const writeTip = place => {
    getRealm().write(() => {
        getRealm().create(schemaName, createRealmTip(place), true);
    });
};

const getTipsByPlaceId = placeId => {
    return retrieveAllTips().filter(tip => tip.placeId == placeId);
};

const getTipById = id => {
    return retrieveAllTips().find(tip => tip.id == id);
};

const getTipByPlaceId = placeId => {
    return retrieveAllTips().find(tip => tip.placeId == placeId);
};

export {
    writeTip,
    retrieveAllTips,
    getTipByPlaceId,
    getTipsByPlaceId,
};
