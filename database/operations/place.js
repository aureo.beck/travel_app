import { getRealm } from './../realm';

const schemaName = 'Place';

const createRealmPlace = ({ id, placeId, title, description, rating, imageUrl, lat, lng, tips, questions }) => {
    return {
        id: String(id),
        title,
        description,
        rating,
        imageUrl,
        lat,
        lng,
        tips,
        questions,
    };
};

// TODO: USE CORRECT QUERY
const retrieveAllPlaces = () => {
    return getRealm() && getRealm().objects(schemaName) || [];
};

const writePlace = place => {
    getRealm().write(() => {
        getRealm().create(schemaName, createRealmPlace(place), true);
    });
};

const writePlaces = places => {
    places.forEach(place => {
        writePlace(place);
    });
}

const getPlace = id => {
    return retrieveAllPlaces().find(place => place.id == id);
};

const deleteAllPlaces = () => {
    getRealm().delete(retrieveAllPlaces());
};

export {
    writePlaces,
    writePlace,
    getPlace,
    deleteAllPlaces,
    retrieveAllPlaces,
};
