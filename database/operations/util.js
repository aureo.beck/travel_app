export function update(objectDb, modifiedObject) {
    for (let property in modifiedObject) {
        objectDb[property] = modifiedObject[property];
    }
}
