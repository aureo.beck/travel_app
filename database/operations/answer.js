
import { getRealm } from './../realm';

const schemaName = 'Answer';

const createRealmAnswer = ({ id, questionId, title, description, rating }) => {
    return {
        id: String(id),
        questionId: String(questionId),
        title,
        description,
        rating,
    };
};

// TODO: USE CORRECT QUERY
const retrieveAnswers = () => {
    return getRealm().objects(schemaName);
};

const deleteAnswers = () => {
    getRealm().delete(retrieveAnswers());
};

const writeAnswer = answer => {
    getRealm().write(() => {
        getRealm().create(schemaName, createRealmAnswer(answer), true);
    });
};

const getAnswer = id => {
    return retrieveAnswers().find(answer => answer.id == id);
};

const getAnswersByQuestionId = questionId => {
    return retrieveAnswers().filter(answer => answer.questionId == questionId);
};

export {
    writeAnswer,
    getAnswer,
    getAnswersByQuestionId,
};
