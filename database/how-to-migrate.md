# Para realizar migração

1 - Criar pasta schemas-vX;

2 - Importar pasta schemas-vX;

3 - Criar arquivo migration-vX.js;

4 - Importar arquivo migration-vX.js ';

5 - Incrementar SCHEMA_VERSION

## 1 - Criando pasta schemas-vX

Dentro da pasta 'app/database/schemas', criar pasta com o nome 'schemas-vX', onde X' é o número da próxima versão do banco de dados,
ficando 'app/database/schemas/schemas-vX'. Copiar arquivos de schema da versão anterior a migração e colocar na pasta 'schemas-vX'.
Dentro dos arquivos de schema copiados, efetuar mudanças referentes a nova versão. Também é possível criar novos schemas.
CUIDADO: NÃO ALTERAR MODELO DAS VERSÕES ANTERIORES QUE JÁ FORAM DISPONIBILIZADAS PARA OS CLIENTES. O modelo anterior é
necessário pois clientes que 'pulam' versões do aplicativo devem fazer sequencialmente as migrações para os modelos anteriores.

## 2 - Importando pasta schemas-vX

Dentro do arquivo 'index.js' localizado em 'app/database/schemas/index.js', importar e exportar da pasta 'schemas-vX'.

## 3 - Criando arquivo migration-vX.js

Dentro da pasta 'app/database/migrations', criar arquivo com o nome 'migration-vX.js', onde X' é o número da próxima versão do 
banco de dados, ficando 'app/database/migrations/migration-vX.js'. Dentro do arquivo, exportar função 'migrate'. Nela será
possível ver o Realm anterior a migração e o futuro Realm. O futuro Realm possui os mesmos dados que o anterior.
É possível popular novas variáveis do novo Realm com dados do anterior. Para mais, ver https://realm.io/docs/javascript/latest/#migrations.
Adicionar comentários explicando o motivo da migração e as mudanças.  

## 4 - Importando arquivo migration-vX.js

Dentro do arquivo 'index.js' localizado em 'app/database/migrations/index.js', importar e exportar arquivo migration-vX.js.

## 5 - Incrementando SCHEMA_VERSION

Dentro deste 'version.js' localizado em 'app/database', incrementar variável exportada SCHEMA_VERSION.
Adicionar comentário resumindo as mudanças da versão. Útil para rastrear bugs que podem parecer ser da migração.
