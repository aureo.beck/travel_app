import * as placeOperations from './operations/place';
import * as questionOperations from './operations/question';
import * as tipOperations from './operations/tip';
import * as answerOperations from './operations/answer';
import * as configOperations from './operations/config';

import { init, deleteAll, stop } from './realm';

export default {
    ...placeOperations,
    ...questionOperations,
    ...tipOperations,
    ...answerOperations,
    ...configOperations,
    init,
    deleteAll,
    stop,
};
