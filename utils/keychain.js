import * as Keychain from 'react-native-keychain';
import { Platform } from 'react-native';

import uuid from 'uuid';

function formatToHex(key) {
    return key.reduce((txt, value) => txt + ('0' + (value & 0xFF).toString(16)).slice(-2), '');
}

const formatForKeychain = Platform.select({
    ios: key => {
        return formatToHex(key);
    },
    android: key => {
        const normalizedMap = JSON.stringify(key).replace('{', '').replace('}', '').split(',').map(item => item.split(':')[1]);
        return formatToHex(normalizedMap);
    },
});

function formatForApp(textKey) {
    return textKey.match(/.{1,2}/g)
        .reduce((buffer, value, index) => {
            buffer[index] = parseInt(value, 16);
            return buffer;
        }, new Int8Array(64));
}

class KeychainUtils {

    async getKey() {
        let key = await this._getKeyFromKeychain();
        if (!key) {
            key = await this._generateKey();
        }
        return key;
    }

    async _getKeyFromKeychain() {
        let key = null;

        try {
            const data = await Keychain.getGenericPassword();
            if (data && data.password) {
                console.info('[keychain-utils] - Dados carregados com sucesso ');
                key = formatForApp(data.password);
            }
        } catch (error) {
            console.error('[keychain-utils] - Não foi possivel acessar os valores no keychain', error);
            throw new Error('Erro ao acessar chave de criptografia no keychain');
        }

        return key;
    }

    async _saveKey(data) {
        try {
            await Keychain.setGenericPassword(data.options, data.key);
            const key = await this._getKeyFromKeychain();
            console.info('[keychain-utils] - Dados salvos com sucesso!');
            return key;
        } catch (error) {
            console.error('[keychain-utils] - Erro ao salvar dados:', error);
            throw new Error('Erro ao salvar chave de criptografia no keychain');
        }
    }

    async _generateKey() {
        const storagedKey = await this._getKeyFromKeychain();

        if (storagedKey) {
            throw new Error('Não é possível criar uma chave de criptografia quando já existe uma armazenada.');
        }
        const uuidV4 = uuid.v4;

        const generatedkey = new Int8Array(64);
        uuidV4(null, generatedkey, 0);
        uuidV4(null, generatedkey, 16);
        uuidV4(null, generatedkey, 32);
        uuidV4(null, generatedkey, 48);

        const data = {
            key: formatForKeychain(generatedkey),
            options: '{}',
        };
        return this._saveKey(data);
    }

    async removeKey() {
        try {
            await Keychain.resetGenericPassword();
        } catch (error) {
            console.error('[Keychain] - Não foi possível resetar a chave de criptografia: ', error);
            throw new Error(error);
        }
    }
}

const instance = new KeychainUtils();
export default instance;
