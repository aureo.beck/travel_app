import { stringsPt } from "../strings/strings-pt"
import { stringsEn } from "../strings/strings-en";

const getString = (language, id) => {
    let strings;
    switch(language) {
        case 'pt':
            strings = stringsPt;
            break;
        case 'en':
            strings = stringsEn;
            break;
    }
    return strings[id];
}

export default getString;