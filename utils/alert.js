import { Alert } from 'react-native';

const showAlert = (title, message) => {
    Alert.alert(
        title,
        message,
        [{ text: 'OK' }],
        { cancelable: true }
    )
}

const showActionAlert = (title, message, okLabel, okAction, cancelLabel, cancelAction) => {
    let actions = [
        { text: okLabel, onPress: okAction },
    ];

    if (cancelLabel) {
        actions.splice(0, 0, { text: cancelLabel, onPress: cancelAction, style: 'cancel' });
    }

    Alert.alert(
        title,
        message,
        actions,
        { cancelable: false }
    )
}

const showDataLossAlert = (nextAction) => {
    Alert.alert(
        'Cuidado!',
        'Você perderá todos os dados inseridos, tem certeza que deseja voltar?',
        [
            { text: 'NÃO', style: 'cancel' },
            { text: 'SIM', onPress: () => nextAction() },
        ],
        { cancelable: true }
    )
}

export {
    showAlert,
    showActionAlert,
    showDataLossAlert,
};