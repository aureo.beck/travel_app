/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';
import AppContainer from './AppContainer';

AppRegistry.registerComponent(appName, () => AppContainer);
