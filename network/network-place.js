import { DEFAULT_HEADERS, PRIMARY_URL } from "../constants/api";

const postPlace = placeObject => new Promise((resolve, reject) => {
    fetch(PRIMARY_URL + '/place',
        {
            method: "POST",
            headers: DEFAULT_HEADERS,
            body: JSON.stringify(placeObject),
        })
        .then(res => res.json())
        .then(data => resolve(data._id))
        .catch(err => reject(err))
});

const getPlacesById = id => new Promise((resolve, reject) =>
    fetch(PRIMARY_URL + '/place',
        {
            method: "GET",
            headers: {
                ...DEFAULT_HEADERS,
                'id': id,
            },
        })
        .then(res => res.json())
        .then(data => resolve(data))
        .catch(err => reject(err)));

const editPlace = (placeObject, id) => new Promise((resolve, reject) => {
    fetch(PRIMARY_URL + '/place',
        {
            method: "PUT",
            headers: {
                ...DEFAULT_HEADERS,
                id,
            },
            body: JSON.stringify(placeObject),
        })
        .then(res => res.json())
        .then(data => resolve(data))
        .catch(err => reject(err))
});

const deletePlace = id => new Promise((resolve, reject) => {
    fetch(PRIMARY_URL + '/place',
        {
            method: "DELETE",
            headers: {
                ...DEFAULT_HEADERS,
                id,
            },
        })
        .then(res => res.json())
        .then(data => resolve(data))
        .catch(err => reject(err))
});

export {
    postPlace,
    getPlacesById,
    editPlace,
    deletePlace,
};