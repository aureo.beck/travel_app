import { DEFAULT_HEADERS, PRIMARY_URL } from "../constants/api";

const postTip = tipObject => new Promise((resolve, reject) => {
    fetch(PRIMARY_URL + '/tip',
        {
            method: "POST",
            headers: DEFAULT_HEADERS,
            body: JSON.stringify(tipObject),
        })
        .then(res => res.json())
        .then(data => resolve(data._id))
        .catch(err => reject(err))
});

const getTipsById = id => new Promise((resolve, reject) =>
    fetch(PRIMARY_URL + '/tip',
        {
            method: "GET",
            headers: {
                ...DEFAULT_HEADERS,
                'id': id,
            },
        })
        .then(res => res.json())
        .then(data => resolve(data))
        .catch(err => reject(err)));

const getTipsByPlaceId = placeId => new Promise((resolve, reject) =>
    fetch(PRIMARY_URL + '/tip',
        {
            method: "GET",
            headers: {
                ...DEFAULT_HEADERS,
                'placeid': placeId,
            },
        })
        .then(res => res.json())
        .then(data => resolve(data))
        .catch(err => reject(err)));

const editTip = (tipObject, id) => new Promise((resolve, reject) => {
    fetch(PRIMARY_URL + '/tip',
        {
            method: "PUT",
            headers: {
                ...DEFAULT_HEADERS,
                id,
            },
            body: JSON.stringify(tipObject),
        })
        .then(res => res.json())
        .then(data => resolve(data))
        .catch(err => reject(err))
});

export {
    postTip,
    getTipsById,
    getTipsByPlaceId,
    editTip,
};