import { DEFAULT_HEADERS, PRIMARY_URL } from "../constants/api";

const postQuestion = questionObject => new Promise((resolve, reject) => {
    fetch(PRIMARY_URL + '/question',
        {
            method: "POST",
            headers: DEFAULT_HEADERS,
            body: JSON.stringify(questionObject),
        })
        .then(res => res.json())
        .then(data => resolve(data._id))
        .catch(err => reject(err))
});

const getQuestionsById = id => new Promise((resolve, reject) =>
    fetch(PRIMARY_URL + '/question',
        {
            method: "GET",
            headers: {
                ...DEFAULT_HEADERS,
                'id': id,
            },
        })
        .then(res => res.json())
        .then(data => resolve(data[0]))
        .catch(err => reject(err)));

const getQuestionByPlaceId = placeId => new Promise((resolve, reject) =>
    fetch(PRIMARY_URL + '/question',
        {
            method: "GET",
            headers: {
                ...DEFAULT_HEADERS,
                'placeid': placeId,
            },
        })
        .then(res => res.json())
        .then(data => resolve(data))
        .catch(err => reject(err)));

const editQuestion = (questionObject, id) => new Promise((resolve, reject) => {
    fetch(PRIMARY_URL + '/question',
        {
            method: "PUT",
            headers: {
                ...DEFAULT_HEADERS,
                id,
            },
            body: JSON.stringify(questionObject),
        })
        .then(res => res.json())
        .then(data => resolve(data))
        .catch(err => reject(err))
});

export {
    postQuestion,
    getQuestionsById,
    getQuestionByPlaceId,
    editQuestion,
};