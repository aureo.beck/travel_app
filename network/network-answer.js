import { DEFAULT_HEADERS, PRIMARY_URL } from "../constants/api";

const postAnswer = answerObject => new Promise((resolve, reject) => {
    fetch(PRIMARY_URL + '/answer',
        {
            method: "POST",
            headers: DEFAULT_HEADERS,
            body: JSON.stringify(answerObject),
        })
        .then(res => res.json())
        .then(data => resolve(data._id))
        .catch(err => reject(err))
});

const getAnswersById = id => new Promise((resolve, reject) =>
    fetch(PRIMARY_URL + '/answer',
        {
            method: "GET",
            headers: {
                ...DEFAULT_HEADERS,
                'id': id,
            },
        })
        .then(res => res.json())
        .then(data => resolve(data))
        .catch(err => reject(err)));

const getAnswersByQuestionId = questionId => new Promise((resolve, reject) =>
    fetch(PRIMARY_URL + '/answer',
        {
            method: "GET",
            headers: {
                ...DEFAULT_HEADERS,
                'questionid': questionId,
            },
        })
        .then(res => res.json())
        .then(data => resolve(data))
        .catch(err => reject(err)));

const editAnswer = (answerObject, id) => new Promise((resolve, reject) => {
    fetch(PRIMARY_URL + '/answer',
        {
            method: "PUT",
            headers: {
                ...DEFAULT_HEADERS,
                id,
            },
            body: JSON.stringify(answerObject),
        })
        .then(res => res.json())
        .then(data => resolve(data))
        .catch(err => reject(err))
});

export {
    postAnswer,
    getAnswersById,
    getAnswersByQuestionId,
    editAnswer,
};