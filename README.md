# Scapy

## Installation

```
npm install
```

## Running

```sh
# Android
react-native run-android
# iOS
react-native run-ios
```

