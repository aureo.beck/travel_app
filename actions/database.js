import Keychain from '../utils/keychain';
import database from '../database';

async function startDatabase(dispatch) {
    let key;
    try {
        key = await Keychain.getKey();
    } catch (error) {
        console.error('Não foi possível carregar a chave de criptografia', error);
        throw {
            response: {
                status: types.ERROR_CRYPTO_KEY,
                message: 'Erro na chave de criptografia',
            },
        };
    }

    try {
        await database.init(key, () => {}, () => {});
    } catch (error) {
        console.error('Não foi possível iniciar o database', error);
        throw {
            response: {
                status: types.ERROR_DATABASE_INIT,
                message: 'Erro ao criar banco de dados',
            },
        };
    }
}

export {
    startDatabase,
};
