import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { dark_grey, fontPrimaryColor } from '../../constants/colors';

import React from 'react';

const AddButton = ({ onPress }) => {
	return (
		<TouchableOpacity onPress={onPress}>
			<View style={styles.addButton}>
				<Text style={styles.description}>{'+'}</Text>
			</View>
		</TouchableOpacity>
	);
}

const styles = StyleSheet.create({
	description: {
		fontSize: 15,
		color: fontPrimaryColor,
	},
	addButton: {
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: dark_grey,
		padding: 1,
		borderRadius: 8,
		marginBottom: 1,
		marginTop: 5,
		height: 30,
	},
});

export default AddButton;
