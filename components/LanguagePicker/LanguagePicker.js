import React from "react";
import { StyleSheet, View, Text, TouchableWithoutFeedback, FlatList } from "react-native";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { updateLanguage } from '../../stores/config/updateLanguage';
import { dark_grey, light_grey, fontPrimaryColor, backgroundColor } from "../../constants/colors";
import getString from "../../utils/getString";
import NavigationButtons from "../NavigationButtons/NavigationButtons";

const LanguagePicker = ({ config: { language }, updateLanguage, closeModal }) => {
  const onLanguageSelection = (item) => {
		updateLanguage(item.value)
		closeModal();
	};

  const renderLanguagePickerItem = ({ item }) => {
		return (
			<View style={[styles.itemContainer, { backgroundColor: language === item.value ? dark_grey : light_grey }]}>
				<TouchableWithoutFeedback onPress={() => onLanguageSelection(item)}>
					<Text style={styles.mainItemTitle}>{item.label}</Text>
				</TouchableWithoutFeedback>
			</View>
		);
	};

  return (
    <View style={styles.languagePickerModal}>
      <FlatList
        data={[
          { value: "pt", label: getString(language, "portuguese") },
          { value: "en", label: getString(language, "english") },
        ]}
        renderItem={renderLanguagePickerItem}
      />
      <NavigationButtons
        previousTitle={"Voltar"}
        previousAction={closeModal}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  itemContainer: {
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: light_grey,
		padding: 5,
		borderRadius: 8,
		marginBottom: 5,
		marginTop: 5,
	},
  languagePickerModal: {
		width: '100%',
		height: 200,
		backgroundColor: backgroundColor,
		padding: 10,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 8,
		borderColor: 'rgba(0, 0, 0, 0.1)',
	},
  mainItemTitle: {
		fontSize: 17,
		color: fontPrimaryColor,
		width: '100%'
	},
});

const mapDispatchToProps = dispatch => (
	bindActionCreators({
		updateLanguage,
	}, dispatch)
);

const mapStateToProps = (state) => {
	const { config } = state;
	return { config };
};

export default connect(mapStateToProps, mapDispatchToProps)(LanguagePicker);
