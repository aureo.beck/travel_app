import React, { useEffect, useState } from "react";
import { StyleSheet, View, Text, ScrollView } from "react-native";
import { Actions } from "react-native-router-flux";
import { Toolbar } from "react-native-material-ui";
import ImageSlider from "react-native-image-slider";
import Toast, { DURATION } from "react-native-easy-toast";
import { connect } from "react-redux";

import {
  backgroundColor,
  light_grey,
  fontPrimaryColor,
  fontTitleColor,
  dark_blue,
  dark_grey,
  toolbar_style,
} from "../../constants/colors";
import List from "../List";
import DescriptionBox from "../DescriptionBox";
import { getTipsByPlaceId } from "../../network/network-tip";
import { getQuestionByPlaceId } from "../../network/network-question";
import getString from "../../utils/getString";
import { deletePlace } from "../../network/network-place";

const PlaceDetail = props => {
  const {
    place: { _id, title, description, imageUrl },
    config: { language },
  } = props;

  const [tips, setTips] = useState([]);
  const [questions, setQuestions] = useState([]);
  const [isFavorite, setIsFavorite] = useState(false);

  useEffect(() => {
    onUpdate();
  });

  const onUpdate = async () => {
    const { _id } = props.place;

    const tips = await getTipsByPlaceId(_id);
    const questions = await getQuestionByPlaceId(_id);

	setTips(tips);
	setQuestions(questions);
  };

  const onDeletePlace = async () => {
    const {
      place: { _id },
      config: { language },
    } = props;

    const response = await deletePlace(_id);

    if (response._id) {
      Actions.main();
      showToast(getString(language, "removedPlaceSuccess"));
    } else {
      showToast(getString(language, "removedPlaceFailed"));
    }
  };

  const showToast = (message) => {
    // refs.toast.show(message, DURATION.LENGTH_SHORT);
  };

  return (
    <ScrollView>
      <View style={styles.root}>
        <Toolbar
          leftElement={"arrow-back"}
          centerElement={getString(language, "place")}
          onLeftElementPress={() => Actions.pop()}
          rightElement={"delete"}
          onRightElementPress={() => onDeletePlace()}
          style={toolbar_style}
        />
        <View style={styles.imageContainer}>
          <ImageSlider images={["data:image/jpeg;base64," + imageUrl]} />
        </View>
        <View style={styles.container}>
          <Text style={styles.title}>{title}</Text>
          <DescriptionBox
            text={description}
            showToast={showToast}
            placeId={_id}
            language={language}
          />
          <List
            limit={3}
            parentId={_id}
            type={"tip"}
            title={getString(language, "tips")}
            items={tips}
            onItemsChange={onUpdate}
            onSeeAll={() => Actions.push("fullTipList", { placeId: _id })}
            showToast={showToast}
          />
          <List
            limit={3}
            parentId={_id}
            type={"question"}
            title={getString(language, "questions")}
            items={questions}
            onItemsChange={onUpdate}
            onItemClick={(questionId) =>
              Actions.push("questionDetail", { questionId })
            }
            onSeeAll={() => Actions.push("fullQuestionList", { placeId: _id })}
            showToast={showToast}
          />
        </View>
        {/* <Toast ref="toast" /> */}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: backgroundColor,
  },
  imageContainer: {
    width: "100%",
    height: 200,
  },
  container: {
    margin: 16,
  },
  title: {
    fontSize: 25,
    color: fontTitleColor,
    marginBottom: 20,
  },
  subtitle: {
    fontSize: 20,
    color: fontTitleColor,
    marginBottom: 20,
    marginTop: 20,
  },
  description: {
    fontSize: 15,
    color: fontPrimaryColor,
  },
  descriptionBox: {
    backgroundColor: light_grey,
    padding: 10,
    borderRadius: 8,
  },
});

const mapStateToProps = (state) => {
  const { config } = state;
  return { config };
};

export default connect(mapStateToProps)(PlaceDetail);
