import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native';
import PropTypes from 'prop-types';
import { light_grey, fontPrimaryColor } from '../../constants/colors';

export default class CustomButton extends Component {

	static propTypes = {
		title: PropTypes.string.isRequired,
		onPress: PropTypes.func.isRequired,
		width: PropTypes.oneOfType([
			PropTypes.string.isRequired,
			PropTypes.number.isRequired,
		]),
		height: PropTypes.number.isRequired,
		backgroundColor: PropTypes.string,
		margin: PropTypes.number,
	};

	static defaultProps = {
		backgroundColor: light_grey,
		margin: 10,
	};

	render() {
		const { title, onPress, width, height, backgroundColor, margin } = this.props;

		return (
			<View style={{
				backgroundColor,
				padding: 12,
				margin: 16,
				width: width,
				height: height,
				justifyContent: 'center',
				alignItems: 'center',
				alignSelf: 'center',
				marginTop: margin,
				marginBottom: margin,
				borderRadius: 15,
			}}>
				<TouchableOpacity onPress={onPress}>
					<Text style={styles.text}>{title}</Text>
				</TouchableOpacity>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	text: {
		color: fontPrimaryColor,
		fontWeight: 'bold',
		fontSize: 16,
	},
});

