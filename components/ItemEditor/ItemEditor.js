import React, { useState } from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import { fontPrimaryColor, fontTitleColor, light_grey } from '../../constants/colors';

import NavigationButtons from '../NavigationButtons/NavigationButtons';
import { connect } from 'react-redux';
import getString from '../../utils/getString';
import { postAnswer } from '../../network/network-answer';
import { postQuestion } from '../../network/network-question';
import { postTip } from '../../network/network-tip';
import { showAlert } from '../../utils/alert';

let currentDescription = null;

const ItemEditor = props => {
	const [title, changetitle] = useState('');
	const [description, changedescription] = useState('');

	const { message, config: { language }, onCancel } = props;

	return (
		<View style={styles.root}>
			<View style={styles.container}>
				<Text style={styles.title}>{message}</Text>
				<TextInput
					placeholderTextColor={fontPrimaryColor}
					style={styles.textInput}
					onChangeText={changetitle}
					value={title}
					placeholder={getString(language, 'title')}
					onSubmitEditing={() => currentDescription.focus()}
				/>

				<TextInput
					ref={input => currentDescription = input}
					placeholderTextColor={fontPrimaryColor}
					style={[styles.textInput, { height: 100 }]}
					onChangeText={changedescription}
					value={description}
					placeholder={getString(language, 'description')}
				/>

				<NavigationButtons
					previousTitle={getString(language, 'cancel')}
					previousAction={onCancel}
					nextTitle={getString(language, 'save')}
					nextAction={() => saveItem(props, title, description)}
				/>
			</View>
		</View>
	);
}

const checkFields = (language, title, description) => {
	if (title && description) {
		return true;
	} else {
		showAlert('Ops!', `${getString(language, 'checkFieldsTitle')}:
		${!title ? `\n${getString(language, 'title')}` : ''}${!description ? `\n${getString(language, 'description')}` : ''}`);
		return false;
	}
}

const saveTip = async (parentId, title, description) => {
	return await postTip({
		title,
		description,
		score: 0,
		placeId: parentId,
	})
}

const saveQuestion = async() => {
	const { title, description } = state;
	const { parentId } = props;

	return await postQuestion({
		title,
		description,
		score: 0,
		placeId: parentId,
	})
}

const saveAnswer = async() => {
	const { title, description } = state;
	const { parentId } = props;

	return await postAnswer({
		title,
		description,
		score: 0,
		questionId: parentId,
	})
}

const saveItem = (props, title, description) => {
	if (checkFields(props.language, title, description)) {
		const { type, onCancel, onChange, parentId } = props;

		switch (type) {
			case 'tip':
				saveTip(parentId, title, description);
				break;
			case 'question':
				saveQuestion();
				break;
			case 'answer':
				saveAnswer();
				break;
		}

		onChange();
		onCancel();
	}
}

const styles = StyleSheet.create({
	root: {
		flex: 1,
	},
	container: {
		alignItems: 'center',
		margin: 5,
	},
	title: {
		fontSize: 25,
		color: fontTitleColor,
		marginBottom: 10,
	},
	textInput: {
		width: '100%',
		height: 40,
		borderRadius: 15,
		backgroundColor: light_grey,
		padding: 8,
		marginBottom: 4,
		color: fontPrimaryColor,
	},
});

const mapStateToProps = (state) => {
	const { config } = state;
	return { config };
};

export default connect(mapStateToProps)(ItemEditor);