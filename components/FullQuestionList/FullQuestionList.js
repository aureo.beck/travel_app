import React, { useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Toolbar } from 'react-native-material-ui';
import { connect } from 'react-redux';

import { backgroundColor, toolbar_style } from '../../constants/colors';
import List from '../List';
import { getQuestionByPlaceId } from '../../network/network-question';
import getString from '../../utils/getString';

const FullQuestionList = props => {
	const { placeId, config: { language } } = props;
	const [questions, setQuestions] = useState([]);
	
	useEffect(() => {
		onUpdate(setQuestions, placeId);
	});

	return (
		<View style={styles.root}>
		<Toolbar
			centerElement={getString(language, 'questions')}
			leftElement={'arrow-back'}
			onLeftElementPress={() => Actions.pop()}
			style={toolbar_style}
		/>
		<List
			limit={-1}
			parentId={placeId}
			type={'question'}
			items={questions}
			onItemsChange={onUpdate} />
	</View>
	);
}

const onUpdate = async (setQuestions, placeId) => {
	const questions = await getQuestionByPlaceId(placeId);
	setQuestions(questions);
}

const styles = StyleSheet.create({
	root: {
		flex: 1,
		backgroundColor: backgroundColor,
	},
});

const mapStateToProps = (state) => {
	const { config } = state;
	return { config };
};

export default connect(mapStateToProps)(FullQuestionList);