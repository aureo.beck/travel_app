import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { dark_grey, fontPrimaryColor, light_grey } from '../../constants/colors';

import React from 'react';
import Vote from '../Vote/Vote';

const ListItem = ({item, isFirst, onPress, showToast}) => {
	if (!item) return null;
	const { title, score, _id } = item;

	return (
		<View style={isFirst ? styles.mainItemContainer : styles.itemContainer}>
			<Vote
				initialCount={score}
				onChange={newRating => updateRating(newRating, _id)}
				showToast={showToast} />
			<TouchableOpacity onPress={onPress}>
				<Text style={isFirst ? styles.mainItemTitle : styles.description}>{title}</Text>
			</TouchableOpacity>
		</View>
	);
}

const styles = StyleSheet.create({
	description: {
		fontSize: 15,
		color: fontPrimaryColor,
	},
	mainItemContainer: {
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: dark_grey,
		padding: 8,
		borderRadius: 8,
	},
	itemContainer: {
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: light_grey,
		padding: 5,
		borderRadius: 8,
		marginBottom: 5,
		marginTop: 5,
	},
	mainItemTitle: {
		fontSize: 17,
		color: fontPrimaryColor,
	},
});

export default ListItem;
