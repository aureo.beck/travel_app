import { StyleSheet, View } from "react-native";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import React from "react";

import NavigationButtons from "../NavigationButtons/NavigationButtons";
import getString from "../../utils/getString";
import { GOOGLE_GEO_API_KEY } from '../../constants/api';
import { dark_grey, fontPrimaryColor, backgroundColor } from "../../constants/colors";

const PlacesSearch = ({ language, onPress, closeModal }) => {
	return (
		<View style={styles.searchPlacesModal}>
		<GooglePlacesAutocomplete
			query={{ key: GOOGLE_GEO_API_KEY }}
			placeholder={getString(language, "locationPickerTitle")}
			minLength={2}
			autoFocus={true}
			returnKeyType={"default"}
			fetchDetails={true}
			onPress={onPress}
			styles={{
			container: styles.googleSearchInputContainer,
			textInputContainer: styles.googleSearchTextInputContainer,
			textInput: styles.googleSearchTextInput,
			predefinedPlacesDescription: styles.googleSearchPlacesDescription,
			}}
			currentLocation={false}
		/>
		<NavigationButtons
			previousTitle={getString(language, "back")}
			previousAction={closeModal}
		/>
		</View>
	);
};

const styles = StyleSheet.create({
  searchPlacesModal: {
    width: "100%",
    height: 300,
    backgroundColor: backgroundColor,
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 8,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  googleSearchInputContainer: {
    width: 300,
  },
  googleSearchTextInputContainer: {
    backgroundColor: "rgba(0,0,0,0)",
    borderTopWidth: 0,
    borderBottomWidth: 0,
  },
  googleSearchTextInput: {
    marginLeft: 0,
    marginRight: 0,
    height: 38,
    color: "#5d5d5d",
    fontSize: 16,
  },
  googleSearchPlacesDescription: {
    color: "#1faadb",
  },
});

export default PlacesSearch;
