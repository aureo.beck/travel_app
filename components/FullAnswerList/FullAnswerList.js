import React, { useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { backgroundColor, toolbar_style } from '../../constants/colors';

import { Actions } from 'react-native-router-flux';
import List from '../List';
import { Toolbar } from 'react-native-material-ui';
import { connect } from 'react-redux';
import { getAnswersByQuestionId } from '../../network/network-answer';
import getString from '../../utils/getString';

const FullAnswerList = props => {
	const { questionId, config: { language } } = props;
	const [answers, setAnswers] = useState([]);
	
	useEffect(() => {
		onUpdate(questionId, setAnswers);
	});

	return (
		<View style={styles.root}>
		<Toolbar
			centerElement={getString(language, 'answers')}
			leftElement={'arrow-back'}
			onLeftElementPress={Actions.pop}
			style={toolbar_style}
		/>
		<List
			limit={-1}
			parentId={questionId}
			type={'answer'}
			items={answers}
			onItemsChange={onUpdate} />
	</View>
	);
}

const onUpdate = async (questionId, setAnswers) => {
	const answers = await getAnswersByQuestionId(questionId);
	setAnswers(answers);
}

const styles = StyleSheet.create({
	root: {
		flex: 1,
		backgroundColor: backgroundColor,
	},
});

const mapStateToProps = (state) => {
	const { config } = state;
	return { config };
};

export default connect(mapStateToProps)(FullAnswerList);