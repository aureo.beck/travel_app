import React from 'react';
import { StyleSheet, View } from 'react-native';
import CustomButton from '../CustomButton';

const NavigationButtons = ({ nextTitle, nextAction, previousTitle, previousAction }) => {
	const mainButtonWidth = previousTitle ? '40%' : '80%';

	return (
		<View style={styles.root}>
			{previousTitle && <CustomButton width={'40%'} height={40} onPress={previousAction} title={previousTitle} />}
			{nextTitle && <CustomButton width={mainButtonWidth} height={40} onPress={nextAction} title={nextTitle} />}
		</View>
	);
}

const styles = StyleSheet.create({
	root: {
		justifyContent: 'center',
		flexDirection: 'row',
		width: '100%'
	},
});

export default NavigationButtons;