import { FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import React, { useState } from 'react';

import AddButton from '../AddButton';
import ItemEditor from '../ItemEditor';
import ListItem from '../ListItem/ListItem';
import Modal from 'react-native-modal';
import NavigationButtons from '../NavigationButtons/NavigationButtons';
import SeeMoreButton from '../SeeMoreButton';
import getSortedArray from '../../utils/getSortedArray';
import getString from '../../utils/getString';
import { backgroundColor, dark_grey, fontPrimaryColor, fontTitleColor, light_grey } from '../../constants/colors';
import { connect } from 'react-redux';

const List = props => {
	const { items, title, limit, showToast, onItemClick, onSeeAll } = props;

	const [isModalOpen, changeisModalOpen] = useState(false);
	const [modalContent, changemodalContent] = useState(null);
	const [modalData, changemodalData] = useState(null);

	const sortedItemsByScore = getSortedArray(items, 'score', false);

	const mainItem = sortedItemsByScore.length > 0 ? sortedItemsByScore[0] : null;
	const filteredItems = limit > 0 ? sortedItemsByScore.slice(1, limit) : sortedItemsByScore.slice(1, sortedItemsByScore.length);

	const onAddButton = () => {
        changeisModalOpen(true);
        changemodalContent("itemEditor");
    }

	const onPressItem = onItemClick ? ({_id}) => onItemClick(_id) : (({title, description}) => {
		changeisModalOpen(true);
		changemodalContent('itemDetail');
		changemodalData({ title, description });
	});

	const onPressSeeMore = () => {
		changeisModalOpen(true);
		changemodalContent('itemEditor');
	};

	return (
    <View style={styles.container}>
      {title && <Text style={styles.subtitle}>{title}</Text>}
      <View style={styles.descriptionBox}>
        <ListItem
          item={mainItem}
          isFirst={true}
          onPress={() => onPressItem(mainItem)}
          showToast={showToast}
        />
        {filteredItems.length > 0 && (
          <FlatList
            data={filteredItems}
            renderItem={({ item }) => (
              <ListItem
                item={item}
                isFirst={false}
                onPress={() => onPressItem(item)}
                showToast={showToast}
              />
            )}
          />
        )}
        {limit > 0 && items.length > limit && (
          <SeeMoreButton onPress={onSeeAll} />
        )}
		<AddButton onPress={onAddButton}/>
      </View>
      <Modal isVisible={isModalOpen}>
        <View style={styles.modal}>
          {modalContent &&
            renderModalContent(
              props,
              modalData,
              modalContent,
              changemodalContent,
              changeisModalOpen
            )}
        </View>
      </Modal>
    </View>
  );
}

const renderItemDetail = (modalData, changeisModalOpen, changemodalContent) => {
	const { title, description } = modalData;

	return (
		<View>
			<Text style={[styles.title, { textAlign: 'center' }]}>{title}</Text>
			<View style={styles.descriptionBox}>
				<Text style={styles.description}>{description}</Text>
			</View>

			<NavigationButtons
				nextTitle={'OK'}
				nextAction={() => {
					changeisModalOpen(false);
					changemodalContent(null);
				}} />
		</View>
	);
}

const renderItemEditor = (props, changeisModalOpen, changemodalContent) => {
	const { type, parentId, onItemsChange } = props;

	return (
		<ItemEditor
			parentId={parentId}
			type={type}
			onChange={onItemsChange}
			message={'New Item'}
			onSubmit={() => { }}
			onCancel={() => {
				changeisModalOpen(false);
				changemodalContent(null);
			}} />
	);
}

const renderModalContent = (props, modalData, modalContent, changemodalContent, changeisModalOpen) => {
	switch (modalContent) {
		case 'itemEditor':
			return renderItemEditor(props, changeisModalOpen, changemodalContent);
		case 'itemDetail':
			return renderItemDetail(modalData, changeisModalOpen, changemodalContent);
	}
}

const styles = StyleSheet.create({
	container: {
		margin: 8
	},
	title: {
		fontSize: 25,
		color: fontTitleColor,
		marginBottom: 20,
	},
	subtitle: {
		fontSize: 20,
		color: fontTitleColor,
		marginBottom: 20,
		marginTop: 20,
	},
	description: {
		fontSize: 15,
		color: fontPrimaryColor,
	},
	descriptionBox: {
		backgroundColor: light_grey,
		padding: 10,
		borderRadius: 8,
	},
	description: {
		fontSize: 15,
		color: fontPrimaryColor,
	},
	descriptionBox: {
		backgroundColor: light_grey,
		padding: 10,
		borderRadius: 8,
	},
	seeMoreButton: {
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: dark_grey,
		padding: 5,
		borderRadius: 8,
		marginTop: 1,
	},
	modal: {
		height: 300,
		backgroundColor: backgroundColor,
		padding: 10,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 8,
		borderColor: 'rgba(0, 0, 0, 0.1)',
	},
});


const mapStateToProps = (state) => {
	const { config } = state;
	return { config };
};

export default connect(mapStateToProps)(List);
