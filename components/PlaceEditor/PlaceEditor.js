import React, { Component } from "react";
import { StyleSheet, View, Text, TextInput, Image } from "react-native";
import LocationView from "react-native-location-view";
import Modal from "react-native-modal";
import ImagePicker from "react-native-image-picker";
import RNFetchBlob from "rn-fetch-blob";
import ImageResizer from "react-native-image-resizer";
import { connect } from "react-redux";

import NavigationButtons from "../NavigationButtons/NavigationButtons";
import { showAlert } from "../../utils/alert";
import { Actions } from "react-native-router-flux";
import {
  backgroundColor,
  light_grey,
  fontPrimaryColor,
  fontTitleColor,
} from "../../constants/colors";
import { GOOGLE_GEO_API_KEY } from "../../constants/api";
import CustomButton from "../CustomButton";
import { postPlace } from "../../network/network-place";
import getString from "../../utils/getString";

const options = {
  title: "Select Image",
  storageOptions: {
    skipBackup: true,
    path: "images",
  },
};

const PlaceEditor = (props) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [isLocationPickerVisible, setIsLocationPickerVisible] = useState(false);
  const [latitude, setLatitude] = useState(null);
  const [longitude, setLongitude] = useState(null);
  const [address, setAddress] = useState(null);
  const [imageData, setImageData] = useState(null);

  const checkFields = () => {
    if (title && description && latitude && longitude) {
      return true;
    } else {
      showAlert(
        "Ops!",
        `You forgot to fill these fields:
			${!title ? "\nTitle" : ""}${!description ? "\nDescription" : ""}${
          !imageData ? "\nImage" : ""
        }${!latitude || !longitude ? "\nLocation" : ""}`
      );
      return false;
    }
  };

  const savePlace = async () => {
    return await postPlace({
      title,
      description,
      score: 0,
      imageUrl: imageData,
      lat: latitude,
      lng: longitude,
    }).then((data) => console.log("data", data));
  };

  const onSave = async () => {
    if (checkFields()) {
      await savePlace();
      Actions.main();
    }
  };

  const onLocationSelect = ({ latitude, longitude, address }) => {
    setLatitude(latitude);
    setLongitude(longitude);
    setAddress(address);
    setIsLocationPickerVisible(false);
  };

  const onOpenImagePicker = () => {
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        ImageResizer.createResizedImage(
          response.uri,
          300,
          200,
          "JPEG",
          60,
          0,
          null
        )
          .then((resizedImageFile) => {
            RNFetchBlob.fs
              .readFile(resizedImageFile.uri, "base64")
              .then(setImageData);
          })
          .catch((err) => {
            console.log("resize error", err);
          });
      }
    });
  };

  const { language } = props.config;

  return (
    <View style={styles.root}>
      <View style={styles.container}>
        <TextInput
          ref={(input) => (title = input)}
          placeholderTextColor={fontTitleColor}
          style={styles.textInput}
          onChangeText={setTitle}
          value={title}
          placeholder={getString(language, "title")}
          onSubmitEditing={() => description.focus()}
        />

        <TextInput
          ref={(input) => (description = input)}
          placeholderTextColor={fontTitleColor}
          style={styles.textInput}
          onChangeText={setDescription}
          value={description}
          placeholder={getString(language, "description")}
        />
        {address && <Text>{address}</Text>}
        {!isLocationPickerVisible && (
          <CustomButton
            margin={5}
            width={"100%"}
            height={40}
            onPress={() => setIsLocationPickerVisible(false)}
            title={
              address
                ? getString(language, "editLocation")
                : getString(language, "insertLocation")
            }
          />
        )}
        {isLocationPickerVisible && (
          <Modal isVisible={isLocationPickerVisible}>
            <View style={{ width: "100%", height: 400 }}>
              <LocationView
                apiKey={GOOGLE_GEO_API_KEY}
                onLocationSelect={onLocationSelect}
                actionText={getString(language, "save")}
                initialLocation={{
                  latitude: -27.600493,
                  longitude: -48.520518,
                }}
                actionButtonStyle={styles.locationActionButton}
                actionTextStyle={styles.locationActionButtonText}
              />
            </View>
          </Modal>
        )}
        {imageData && (
          <Image
            source={{ uri: "data:image/jpeg;base64," + imageData }}
            style={styles.imagePreview}
            resizeMode="cover"
          />
        )}
        <CustomButton
          margin={5}
          width={"100%"}
          height={40}
          onPress={onOpenImagePicker}
          title={imageData ? "Edit image" : "Insert image"}
        />
      </View>

      <NavigationButtons
        nextTitle={getString(language, "save")}
        nextAction={onSave}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: backgroundColor,
  },
  container: {
    alignItems: "center",
    margin: 16,
  },
  title: {
    fontSize: 25,
    color: fontTitleColor,
    marginBottom: 20,
  },
  subtitle: {
    fontSize: 20,
    color: fontTitleColor,
    marginBottom: 20,
    marginTop: 20,
  },
  description: {
    fontSize: 15,
    color: fontTitleColor,
  },
  descriptionBox: {
    backgroundColor: light_grey,
    padding: 10,
    borderRadius: 8,
  },
  textInput: {
    width: "100%",
    height: 40,
    borderRadius: 15,
    backgroundColor: light_grey,
    padding: 8,
    marginBottom: 4,
    color: fontPrimaryColor,
  },
  image: {
    flex: 3,
    width: "100%",
    height: 200,
    alignSelf: "center",
  },
  locationActionButton: {
    backgroundColor: "#212121",
    padding: 12,
    margin: 16,
    width: "80%",
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    marginTop: 10,
    marginBottom: 10,
    borderRadius: 15,
  },
  locationActionButtonText: {
    color: "white",
    fontSize: 20,
  },
  imagePreview: {
    width: "100%",
    height: 200,
    alignSelf: "center",
  },
});

const mapStateToProps = (state) => {
  const { config } = state;
  return { config };
};

export default connect(mapStateToProps)(PlaceEditor);