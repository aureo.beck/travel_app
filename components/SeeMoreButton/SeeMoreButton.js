import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { dark_grey, fontPrimaryColor } from '../../constants/colors';

import React from 'react';
import { connect } from 'react-redux';
import getString from '../../utils/getString';

const SeeMoreButton = ({ config, onPress }) => {
	return (
		<TouchableOpacity onPress={onPress}>
			<View style={styles.seeMoreButton}>
				<Text style={styles.description}>{getString(config.language, 'seeAll')}</Text>
			</View>
		</TouchableOpacity>
	);
}

const styles = StyleSheet.create({
	description: {
		fontSize: 15,
		color: fontPrimaryColor,
	},
	seeMoreButton: {
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: dark_grey,
		padding: 5,
		borderRadius: 8,
		marginTop: 1,
	},
});

const mapStateToProps = (state) => {
	const { config } = state;
	return { config };
};

export default connect(mapStateToProps)(SeeMoreButton);
