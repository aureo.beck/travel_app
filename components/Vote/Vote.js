import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native';
import Svg, {
  Polyline,
} from 'react-native-svg';
import { fontTitleColor, black_grey, light_grey, dark_grey, grey_400 } from '../../constants/colors';

const Arrow = ({direction, ...props}) => (
  <Svg height="12" width="28" {...props}>
    <Polyline
      fill={grey_400}
      points={
        direction === 'up' ?
        "0.595,11.211 14.04,1.245 27.485,11.211" :
        "27.485,0.803 14.04,10.769 0.595,0.803"
      }
    />
  </Svg>
)

Arrow.defaultProps = {
  direction: 'up'
}
  
export default class Vote extends Component {
  constructor(props) {
    super(props)
    
    this.state = {
      count: props.initialCount,
      isEnabled: true
    }
  }

  onVote(newCount) {
    const { isEnabled } = this.state;
    const { onChange, showToast } = this.props;

    if (isEnabled) {
      this.setState({ count: newCount, isEnabled: false });
      onChange(newCount)
    } else {
      showToast('You already voted!');
    }
  }

  render() {
    const { count } = this.state;
    
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => this.onVote(count + 1)}>
          <Arrow
            direction="up"
          />
        </TouchableOpacity>
        <View style={styles.textContainer}>
          <Text style={styles.text}>
            {count}
          </Text>
        </View>
        <TouchableOpacity onPress={() => this.onVote(count - 1)}>
          <Arrow
            direction="down"
          />
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    marginRight: 10,
  },
  textContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  text: {
    alignItems: 'center',
		color: fontTitleColor,
		fontSize: 16,
	},
});
