import Geolocation from 'react-native-geolocation-service';
import MapView from 'react-native-maps';
import Modal from 'react-native-modal';
import React, { Component } from 'react';
import turf from 'turf';
import { Actions } from 'react-native-router-flux';
import { FlatList, StyleSheet, Text, View, Animated, Image, Dimensions, TouchableOpacity, TouchableWithoutFeedback, PermissionsAndroid, PanResponder } from "react-native";
import { Icon, Toolbar } from 'react-native-material-ui';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { backgroundColor, fontPrimaryColor, dark_blue, dark_grey, toolbar_style, light_grey } from '../../constants/colors';
import { insertInitialData } from '../../database/initialize';
import { retrieveAllPlaces } from '../../database/operations/place';
import { startDatabase } from '../../actions/database';
import NavigationButtons from '../NavigationButtons/NavigationButtons';
import PlacesSearch from '../PlacesSearch/PlacesSearch';
import LanguagePicker from '../LanguagePicker/LanguagePicker';
import { getPlacesById } from '../../network/network-place';
import { getBoundByRegion } from '../../utils/getBoundByRegion';
import { updateRegion } from '../../stores/map/updateRegion';
import getString from '../../utils/getString';

const markerImage = require('../../assets/images/marker.png');

const { width, height } = Dimensions.get("window");

const CARD_HEIGHT = height / 4;
const CARD_WIDTH = CARD_HEIGHT - 50;
const SLIDER_OFFSET = height / 8;

class Home extends Component {
	constructor(props) {
		super(props);

		const { map: { region } } = props;

		this.state = {
			currentPlaceIndex: 0,
			isNavigating: true,
			latitudeDelta: region.latitudeDelta,
			places: null,
			isModalOpen: false,
			modalType: '',
			pan: new Animated.ValueXY(),
			currentRegion: region,
			allPlaces: null,
		};

		console.disableYellowBox = true;

		this.translateY = new Animated.Value(0),
			this.animation = new Animated.Value(0);
		this.onPlaceCardClick = this.onPlaceCardClick.bind(this);
		this.animateToMapRegion = this.animateToMapRegion.bind(this);
		this.renderMarker = this.renderMarker.bind(this);
		this.navigateToCurrentPosition = this.navigateToCurrentPosition.bind(this);
		this.onSearchLocation = this.onSearchLocation.bind(this);
		this.onGooglePlacesPress = this.onGooglePlacesPress.bind(this);
		this.updatePlacesMarkers = this.updatePlacesMarkers.bind(this);
		this.getPlacesFromApi = this.getPlacesFromApi.bind(this);
		this.onToolbarOptionsOpen = this.onToolbarOptionsOpen.bind(this);
		this.closeModal = this.closeModal.bind(this);
	}

	componentWillMount() {
		this.getPlacesFromApi();

		this._val = { x: 0, y: 0 }
		this.state.pan.addListener((value) => this._val = value);

		this.panResponder = PanResponder.create({
			onStartShouldSetPanResponder: (e, gesture) => true,
			onPanResponderGrant: (e, gesture) => {
				this.state.pan.setOffset({
					x: this._val.x,
					y: this._val.y
				})
				this.state.pan.setValue({ x: 0, y: 0 })
			},
			onPanResponderMove: Animated.event([
				null, { dy: this.translateY }
			]),
			onPanResponderRelease: (e, { dy, moveY }) => {
				if (-dy > 0) {
					this.onToggleSliderIsHidden(false)
				} else if (dy >= 0) {
					this.onToggleSliderIsHidden(true)
				} else {
					Animated.spring(this.translateY, {
						toValue: 0,
						bounciness: 10
					}).start();
				}
			}
		});
	}

	onToggleSliderIsHidden(isHidden) {
		this.setState({ isNavigating: isHidden })

		Animated.timing(this.translateY, {
			toValue: isHidden ? SLIDER_OFFSET / 2 : -SLIDER_OFFSET,
			duration: 200
		}).start();
	}

	renderFooterContainer() {
		const { places } = this.state;
		const hasPlaces = places && places.length > 0;

		const ViewComponent = hasPlaces ? Animated.View : View;

		return (
			<ViewComponent
				style={[hasPlaces && { transform: [{ translateY: this.translateY }] }, styles.footer, { bottom: hasPlaces ? -SLIDER_OFFSET : 0 }]} {...this.panResponder.panHandlers} >
				{this.renderFooter()}
			</ViewComponent>
		);
	}

	async initializeDatabase() {
		await startDatabase()
		retrieveAllPlaces().length == 0 && await insertInitialData();
		const allPlaces = await retrieveAllPlaces()

		this.setState({ places: allPlaces })
	}

	async getPlacesFromApi() {
		const { map: { region } } = this.props;
		const allPlaces = await getPlacesById('');
		await this.setState({ allPlaces })
		this.updatePlacesMarkers(region);
	}

	updatePlacesMarkers(region) {
		this.props.updateRegion(region);

		const { allPlaces } = this.state;
		if (!allPlaces) return;

		const filteredPlaces = allPlaces.filter(place => {
			const { lat, lng } = place;
			const pt = turf.point([lng, lat]);

			const bounds = getBoundByRegion(region);
			const { minLng, minLat, maxLng, maxLat } = bounds;

			var poly = turf.polygon([[
				[minLng, minLat],
				[minLng, maxLat],
				[maxLng, maxLat],
				[maxLng, minLat],
				[minLng, minLat]
			]]);

			return turf.inside(pt, poly);
		});

		this.setState({ places: filteredPlaces });
	}

	componentDidMount() {
		const { places } = this.state;
		if (!places || places.length === 0) return;

		this.animation.addListener(({ value }) => {
			let index = Math.floor(value / CARD_WIDTH + 0.3); // animate 30% away from landing on the next item
			if (index >= places.length) {
				index = places.length - 1;
			}
			if (index <= 0) {
				index = 0;
			}

			clearTimeout(this.regionTimeout);
			this.regionTimeout = setTimeout(() => {
				if (this.state.currentPlaceIndex !== index) {
					this.setState({ currentPlaceIndex: index });
				}
			}, 10);
		});
	}

	animateToMapRegion(coordinate) {
		const { map: { region } } = this.props;

		this.map.animateToRegion(
			{
				...coordinate,
				latitudeDelta: region.latitudeDelta,
				longitudeDelta: region.longitudeDelta,
			},
			350
		);
	}

	renderMap() {
		const { places, isNavigating } = this.state;
		const { map: { region } } = this.props;

		return (
			<TouchableWithoutFeedback onPress={() => !isNavigating && this.onToggleSliderIsHidden(true)}>
				<MapView
					ref={map => this.map = map}
					initialRegion={region}
					style={styles.container}
					onRegionChangeComplete={this.updatePlacesMarkers}
				>
					{places && places.length > 0 && places.map(this.renderMarker)}
				</MapView>
			</TouchableWithoutFeedback>
		);
	}

	renderMarker(place, index) {
		const { places } = this.state;
		if (!places || places.length === 0) return;

		const interpolations = places.map((_, index) => {
			const inputRange = [
				(index - 1) * CARD_WIDTH,
				index * CARD_WIDTH,
				((index + 1) * CARD_WIDTH),
			];
			const scale = this.animation.interpolate({
				inputRange,
				outputRange: [1, 2.5, 1],
				extrapolate: "clamp",
			});
			const opacity = this.animation.interpolate({
				inputRange,
				outputRange: [0.35, 1, 0.35],
				extrapolate: "clamp",
			});
			return { scale, opacity };
		});

		const opacityStyle = {
			opacity: interpolations[index].opacity,
		};

		const { lat, lng } = place;

		return (
			<MapView.Marker
				key={index}
				coordinate={{ latitude: parseFloat(lat), longitude: parseFloat(lng) }}>
				<Animated.View style={[styles.markerWrap, opacityStyle]}>
					<Image
						style={styles.pinMarker}
						source={markerImage}
					/>
				</Animated.View>
			</MapView.Marker>
		);
	}

	renderPlaceSlider() {
		const { places } = this.state;
		if (!places || places.length === 0) return;

		return (
			<Animated.ScrollView
				horizontal
				scrollEventThrottle={1}
				showsHorizontalScrollIndicator={false}
				snapToInterval={CARD_WIDTH}
				onScroll={Animated.event(
					[
						{
							nativeEvent: {
								contentOffset: {
									x: this.animation,
								},
							},
						},
					],
					{ useNativeDriver: true }
				)}
				contentContainerStyle={styles.endPadding}>
				{this.renderPlaces()}
			</Animated.ScrollView>
		);
	}

	onPlaceCardClick(place, index) {
		this.setState({ currentPlaceIndex: index });
		Actions.push('placeDetail', { place });
	}

	renderPlaces() {
		const { currentPlaceIndex, isNavigating, places } = this.state;
		if (!places || places.length === 0) return;

		return places.map((place, index) => (
			<TouchableOpacity onPress={() => isNavigating ? this.onToggleSliderIsHidden(false) : this.onPlaceCardClick(place, index)}>
				<View style={index === currentPlaceIndex ? [styles.card, { backgroundColor: backgroundColor }] : styles.card}>
					<Image
						source={{ uri: 'data:image/jpeg;base64,' + place.imageUrl }}
						style={styles.cardImage}
						resizeMode="cover"
					/>
					<View style={styles.textContent}>
						<Text numberOfLines={1} style={index === currentPlaceIndex ? [styles.cardtitle, { color: fontPrimaryColor }] : styles.cardtitle}>{place.title}</Text>
						<Text numberOfLines={1} style={index === currentPlaceIndex ? [styles.cardDescription, { color: fontPrimaryColor }] : styles.cardDescription}>
							{place.description}
						</Text>
					</View>
				</View>
			</TouchableOpacity>
		));
	}

	renderMapControls() {
		return (
			<View>
				<View style={styles.mapControls}>
					<TouchableOpacity onPress={this.navigateToCurrentPosition}>
						<Icon
							size={40}
							color={dark_blue}
							name="gps-fixed" />
					</TouchableOpacity>
				</View>
				<View style={styles.mapControls}>
					<TouchableOpacity onPress={this.onSearchLocation}>
						<Icon
							size={40}
							color={dark_blue}
							name="edit-location" />
					</TouchableOpacity>
				</View>
			</View>
		);
	}

	renderFooter() {
		return (
			<View>
				{this.renderMapControls()}
				{this.renderPlaceSlider()}
			</View>
		);
	}

	onSearchLocation() {
		this.setState({ isModalOpen: true, modalType: 'places-search' });
	}

	async navigateToCurrentPosition() {
		const { language } = this.props.config;

		await PermissionsAndroid.request(
			PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
			{
				title: getString(language, 'locationPermissionTitle'),
				message: getString(language, 'locationPermissionDescription')
			}
		);

		Geolocation.getCurrentPosition(
			(position) => {
				const { latitude, longitude } = position.coords;
				this.animateToMapRegion({ latitude, longitude });
			},
			(error) => {
				console.log(error.code, error.message);
			},
			{ enableHighAccuracy: true, timeout: 3000, maximumAge: 10000 }
    	);
	}

	closeModal() {
		this.setState({ isModalOpen: false })
	}

	renderModal() {
		const { language } = this.props.config;

		let renderContent;
		switch (this.state.modalType) {
			case 'places-search':
				renderContent = (<PlacesSearch 
					onPress={this.onGooglePlacesPress} 
					language={language} 
					closeModal={this.closeModal}/>);
				break;
			case 'language-picker':
				renderContent = (<LanguagePicker closeModal={this.closeModal}/>);
				break;
		}
		return (
			<Modal isVisible={this.state.isModalOpen}>
				{renderContent}
			</Modal>
		);
	}

	onGooglePlacesPress(_, details = null) {
		const { lat, lng } = details.geometry.location;
		this.setState({ isModalOpen: false });
		this.animateToMapRegion({ latitude: lat, longitude: lng });
	}

	onToolbarOptionsOpen({ index }) {
		switch (index) {
			case 0:
				Actions.push('placeEditor')
				break;
			case 1:
				this.setState({ isModalOpen: true, modalType: 'language-picker' });
				break;
		}
	}

	render() {
		const { language } = this.props.config;

		return (
			<View style={styles.container}>
				<Toolbar
					centerElement="scapy"
					rightElement={{
						menu: {
							icon: "more-vert",
							labels: [getString(language, 'newPlace'), getString(language, 'language')]
						}
					}}
					onRightElementPress={this.onToolbarOptionsOpen}
					style={toolbar_style}
				/>
				{this.renderMap()}
				{this.renderFooterContainer()}
				{this.renderModal()}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	mapControls: {
		alignItems: 'flex-end',
		margin: 5,
	},
	footer: {
		position: "absolute",
		bottom: 0,
		left: 0,
		right: 0,
		paddingVertical: 10,
	},
	endPadding: {
		paddingRight: width - CARD_WIDTH,
	},
	card: {
		padding: 10,
		elevation: 2,
		backgroundColor: "#FFF",
		marginHorizontal: 10,
		shadowColor: "#000",
		shadowRadius: 5,
		shadowOpacity: 0.3,
		shadowOffset: { x: 2, y: -2 },
		height: CARD_HEIGHT,
		width: CARD_WIDTH,
		overflow: "hidden",
	},
	cardImage: {
		flex: 3,
		width: "100%",
		height: "100%",
		alignSelf: "center",
	},
	textContent: {
		flex: 1,
	},
	cardtitle: {
		fontSize: 12,
		marginTop: 5,
		fontWeight: "bold",
	},
	cardDescription: {
		fontSize: 12,
	},
	markerWrap: {
		alignItems: "center",
		justifyContent: "center",
	},
	marker: {
		width: 8,
		height: 8,
		borderRadius: 4,
		backgroundColor: "rgba(130,4,150, 0.9)",
	},
	ring: {
		width: 24,
		height: 24,
		borderRadius: 12,
		backgroundColor: "rgba(130,4,150, 0.3)",
		position: "absolute",
		borderWidth: 1,
		borderColor: "rgba(130,4,150, 0.5)",
	},
	hidden: {
		bottom: -SLIDER_OFFSET,
	},
	searchPlacesModal: {
		width: '100%',
		height: 300,
		backgroundColor: backgroundColor,
		padding: 10,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 8,
		borderColor: 'rgba(0, 0, 0, 0.1)',
	},
	pinMarker: {
		width: 30,
		height: 30,
	},
	itemContainer: {
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: light_grey,
		padding: 5,
		borderRadius: 8,
		marginBottom: 5,
		marginTop: 5,
	},
	mainItemTitle: {
		fontSize: 17,
		color: fontPrimaryColor,
		width: '100%'
	},
});

const mapDispatchToProps = dispatch => (
	bindActionCreators({
		updateRegion,
	}, dispatch)
);

const mapStateToProps = (state) => {
	const { config, map } = state;
	return { config, map };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);