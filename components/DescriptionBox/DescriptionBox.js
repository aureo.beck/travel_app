import React, { useState } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, TextInput, } from 'react-native';
import { light_grey, fontPrimaryColor } from '../../constants/colors';
import { Icon } from 'react-native-material-ui';
import { editPlace } from '../../network/network-place';
import getString from '../../utils/getString';

const DescriptionBox = props => {
	const [isEditing, changeisEditing] = useState(false);
	const [description, changedescription] = useState(props.text);

	return (
		<View style={styles.container}>
			<View style={{ width: '90%' }}>
				{isEditing ?
					<TextInput
						autoFocus
						placeholderTextColor={fontPrimaryColor}
						style={styles.text}
						onChangeText={changedescription}
						value={description}
					/> :
					<Text style={styles.text}>{description}</Text>}
			</View>
			{isEditing ?
				<View style={{ width: '100%' }}>
					<TouchableOpacity onPress={() => onSubmitEditDescription(props, description, changeisEditing)} style={styles.editIconButton}>
						<Icon name="check" size={20} />
					</TouchableOpacity>
					<TouchableOpacity onPress={() => changeisEditing(false)} style={styles.editIconButton}>
						<Icon name="close" size={20} />
					</TouchableOpacity>
				</View> :
				<TouchableOpacity onPress={() => changeisEditing(true)} style={styles.iconButtom}>
					<Icon name="edit" size={20} />
				</TouchableOpacity>
			}
		</View>
	);
}

const onSubmitEditDescription = async (props, description, changeisEditing) => {
	const { placeId, showToast, language } = props;

	const response = await editPlace({ description }, placeId);

	if (response._id) {
		showToast(getString(language, 'editedDescriptionSuccess'));
	} else {
		showToast(getString(language, 'editedDescriptionFailed'));
	}

	changeisEditing(false);
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: light_grey,
		padding: 10,
		borderRadius: 8,
		flexDirection: 'row',
	},
	text: {
		fontSize: 15,
		color: fontPrimaryColor,
	},
	editIconButton: {
		alignItems: 'center',
		width: '10%',
		marginBottom: 10,
	},
	iconButtom: {
		alignItems: 'center',
		width: '10%',
	}
});

export default DescriptionBox;
