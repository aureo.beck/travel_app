import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, ScrollView } from 'react-native';
import { backgroundColor, fontPrimaryColor } from '../../constants/colors';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import List from '../List';
import DescriptionBox from '../DescriptionBox';
import { getQuestionsById } from '../../network/network-question';
import { getAnswersByQuestionId } from '../../network/network-answer';
import getString from '../../utils/getString';

const QuestionDetail = props => {
	const { questionId, config: { language } } = props;

	const [question, changequestion] = useState({});
	const [answers, changeanswers] = useState([]);

	useEffect(() => {
		onUpdate(questionId, changequestion, changeanswers);
	});

	const { title, description } = question;

	return (
		<ScrollView>
			<View style={styles.root}>
				<View style={styles.container}>
					<Text style={styles.title}>{title}</Text>
					<DescriptionBox text={description} />
					<List
						limit={3}
						parentId={questionId}
						type={'answer'}
						title={getString(language, 'answers')}
						items={answers}
						onItemsChange={onUpdate}
						onSeeAll={() => Actions.push('fullAnswerList', { questionId })} />
				</View>
			</View>
		</ScrollView>
	);
}

const onUpdate = async (questionId, changequestion, changeanswers) => {
	const question = await getQuestionsById(questionId)
	const answers = await getAnswersByQuestionId(questionId);

	changequestion(question);
	changeanswers(answers);
}

const styles = StyleSheet.create({
	root: {
		flex: 1,
		backgroundColor: backgroundColor,
	},
	container: {
		margin: 16
	},
	title: {
		fontSize: 25,
		color: fontPrimaryColor,
		marginBottom: 20,
	},
});

const mapStateToProps = (state) => {
	const { config } = state;
	return { config };
};

export default connect(mapStateToProps)(QuestionDetail);
