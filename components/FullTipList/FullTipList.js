import React, { useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Toolbar } from 'react-native-material-ui';
import { connect } from 'react-redux';

import { backgroundColor, toolbar_style } from '../../constants/colors';
import List from '../List';
import { getTipsByPlaceId } from '../../network/network-tip';
import getString from '../../utils/getString';

const FullTipList = props => {
	const { placeId, config: { language } } = props;
	const [tips, setTips] = useState([]);
	
	useEffect(() => {
		onUpdate(setTips, placeId);
	});

	return (
		<View style={styles.root}>
			<Toolbar
				centerElement={getString(language, 'tips')}
				leftElement={'arrow-back'}
				onLeftElementPress={() => Actions.pop()}
				style={toolbar_style}
			/>
			<List
				limit={-1}
				parentId={placeId}
				type={'tip'}
				items={tips}
				onItemsChange={onUpdate} />
		</View>
	);
}

const onUpdate = async (setTips, placeId) => {
	const tips = await getTipsByPlaceId(placeId)
	setTips(tips);
}

const styles = StyleSheet.create({
	root: {
		flex: 1,
		backgroundColor: backgroundColor,
	},
});

const mapStateToProps = (state) => {
	const { config } = state;
	return { config };
};

export default connect(mapStateToProps)(FullTipList);