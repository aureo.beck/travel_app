import React, { Component } from 'react';
import { Provider, connect } from 'react-redux';
import { createStore } from 'redux';

import { Scene, Router, Stack } from 'react-native-router-flux';
import Home from './components/Home';
import PlaceDetail from './components/PlaceDetail';
import PlaceEditor from './components/PlaceEditor';
import { dark_grey, black_grey } from './constants/colors';
import FullTipList from './components/FullTipList';
import FullQuestionList from './components/FullQuestionList';
import QuestionDetail from './components/QuestionDetail';
import FullAnswerList from './components/FullAnswerList';
import getString from './utils/getString';

class App extends Component {

  render() {
    const { config: { language } } = this.props;

    return (
      <Router
        navigationBarStyle={{ backgroundColor: dark_grey }}
        titleStyle={{ color: black_grey }}
        backButtonTextStyle={{ color: black_grey }}
        backButtonTintColor={black_grey} >
        <Stack key="root">
          <Scene key="main" component={Home} title="scapy" hideNavBar initial />
          <Scene key="placeDetail" component={PlaceDetail} title={getString(language, 'place')} hideNavBar />
          <Scene key="placeEditor" component={PlaceEditor} title={getString(language, 'newPlace')} />
          <Scene key="fullTipList" component={FullTipList} title={getString(language, 'tips')} hideNavBar />
          <Scene key="fullQuestionList" component={FullQuestionList} title={getString(language, 'questions')} hideNavBar />
          <Scene key="fullAnswerList" component={FullAnswerList} title={getString(language, 'answers')} hideNavBar />
          <Scene key="questionDetail" component={QuestionDetail} title={getString(language, 'question')} />
        </Stack>
      </Router>
    );
  }
}


const mapStateToProps = (state) => {
  const { config } = state;
  return { config };
};

export default connect(mapStateToProps)(App);